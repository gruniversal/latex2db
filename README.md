# LaTeX2DocBook

This project covers a LaTeX-to-DocBook translater including a LaTeX-to-XML converter (C++) and appropriate stylesheets (XSL).

It was originally created during my diploma thesis. Afterwards I decided to provide the source to the public on Sourceforge. Now it has moved to GitLab for sentimental reasons.

Author:

* David Gruner, https://www.gruniversal.de

License:

* The code is released under the GPL: http://www.gnu.org/copyleft/gpl.html

Therefore you have the freedom to:
  
* use it for any purpose
* distribute a copy of the source-code
* modify the program to your needs
* distribute a copy of your modified version under the GPL license

## What it does ##

See `doc/docs_german.txt`.

## How to use ##

* compile latex2xml (see `cpp/makefile`)
* run latex2xml on your tex-file to create a xml-file
* run a series of xsltproc commands on the xml-file
* the result should be a docbook-file (more or less)

## Requirements ##

* g++
* xsltproc
* make

## Important Notes ##

* all outputs and code comments are in german
* a much more advanced project with a similar approach can be found here: https://math.nist.gov/~BMiller/LaTeXML/manual/

## Roadmap ##

None.

## Changelog ##

### v0.718 (Nov. 2008) ###

* adapted code to run with debian linux

### v0.712 (Feb. 2004) ###

* initial version developed during diploma thesis (Windows)