#include "Xparser.h"

int Xparser::activeDblQuote = false;	

/*** Konstruktor und Destruktor ***/

Xparser::Xparser() {
	//filename = new char[255];
	file_open = 0;
}

Xparser::~Xparser() {
	//delete[] filename;
}

/*** Datei öffnen und schließen ***/

bool Xparser::openFile(string filename) {
	ihandle.open(filename.c_str(), ios_base::in);
	if (!ihandle) {
		closeFile();
	} else {
		this->filename = filename;
		file_open = true;
	}
	return file_open;
}

// File schließen
void Xparser::closeFile() {
	if (file_open) {
		ihandle.close();
		file_open = false;
	}
}

/*** Daten einlesen ***/

// liest ein Zeichen aus dem Stream
char Xparser::readChar() {
	if (file_open) {
		if (ihandle.eof()) {
			inchar = 0;
		} else {
			ihandle.read((char*)&inchar, sizeof(char));
		}
	}
	return inchar;
}

// letztes Zeichen wieder in Stream zurückstellen
void Xparser::resetChar() {
	if (file_open) {
		ihandle.putback(inchar);
	}
}

// Zeichen lesen und zurückstellen
char Xparser::peekChar() {
	char c;
	c = readChar();
	resetChar();
	return c;
}

// ==Debug== mehrere Zeichen auslesen
void Xparser::readChars(char* &s, int maxlength = 255, char delimiter='\n') {
	char c;
	string dummy = "";
	strcpy(s,"");
	if (file_open) {		
		do {
			ihandle.read((char*)&c, sizeof(char));
			dummy += c;
		} while(c != delimiter);		
	}
	strcpy(s,dummy.c_str());
}

// ==Debug== Eine Zeile auslesen
void Xparser::readLine(char* &s, int maxlength = 255) {
	readChars(s, maxlength,'\n');
}

// ==Debug== Integer --> String
string Xparser::intToStr(int number) {
	ostringstream wandler;
	wandler << number;
	return wandler.str();
}

/*** Ausgabefunktionen ***/

// ==Debug== Ausgabe
void Xparser::output(string txt) {
	cout << txt;
}

void Xparser::output(int txt) {
	cout << txt;
}

void Xparser::output(char txt) {
	cout << txt;
}

// Tag schreiben
string Xparser::tag(string name) {
	return "<"+name+"/>";
}
string Xparser::tag(string name, string inner) {
	string txt = "";
	
	txt = trim(inner);
	
	if ((txt != "") && (txt != " ")) {
		txt = "<" + name + ">" + txt + "</" + name + ">";
	} else {
		txt = "";
	}
	return txt;
}

// Text trimmen
string Xparser::trim(string s) {
	string dummy = "", txt = "";
	char c, last = '-';
	int i;

	// nicht druckbare Zeichen -> Space
	for(i = 0; i < s.length(); i++) {
		c = s[i];
		if (c=='\t' || c=='\n' || c=='\r') {
			dummy += ' ';
		} else {
			dummy += c;
		}
	}
	
	// Spaces zusammenschieben
	for(i = 0; i < dummy.length(); i++) {
		c = dummy[i];
		if (c == ' ') {
//			if ((last!=' ') && (i+1!=dummy.length())) {
			if (last != ' ') {
				txt += ' ';
			}
		} else {
			txt += c;
		}
		last = c;
	}
	
	return txt;
}

/*** Pakete registrieren ***/
int Xparser::usePackage(string package) {
	//output("Package: "+package+"\n");
	
	if ((package == "ngerman") or (package == "german"))	{
		activeDblQuote = true;
	}
	
}

/*** Konvertierung starten ***/
int Xparser::convert(string filename) {
	string txt = "";
	int count = 0;
	ohandle.open(filename.c_str(), ios_base::out | ios::trunc);
	if (!ohandle) {
		cout << "FEHLER: Ausgabedatei konnte nicht geöffnet werden! \n";
		count = -1;
	} else {
		txt = "<?xml version='1.0' encoding='ISO-8859-1'?>\n";
		txt += tag("latexxml",parse());
		count = txt.length();
		ohandle.write(txt.c_str(),count);
	}
	ohandle.close();
	return count;
}

/*** Parsermodi ***/

// ==Parser== [DEFAULT]
// return zwingend ohne Tags !
string Xparser::parse() {

	string satz = "";	// tatschlicher Ausgabestrom
	string wort = "";	// Testpuffer in dieser Funktion
	char c;

	output("parse " + filename + " .. ");

	do {
		c = readChar();	
		switch (c) {
	
			/// Fälle Klammern -> [PARAMxx] ///
			case '{'	:
				satz += trim(tag("text", wort));
				wort = "";
				satz += parseParameter(c);
			break;						
	 
			/// Fall 0 -> End of File ///
			case 0 :
				satz += trim(tag("text", wort));
				wort = "";
			break;

			// +++ die folgenden Fälle sind auch in ::parseParameter !!! +++ //

			/// Fälle '\' -> [COMMAND] oder Sonderzeichen ///
			case '\\' :
				satz += trim(tag("text", wort));
				wort = "";
				satz += parseCommandOrSpecialChars();
			break;

			/// Fall '%' -> [COMMENT] ///
			case '%' :
				satz += trim(tag("text", wort));
				wort = "";
				satz += parseComment();
			break;

			/// Fälle '"' -> Umlaute unter german ///
			case '"' :
				if (activeDblQuote==true) {
					char c;
					string dummy;
					c = readChar();
					switch (c) {
						// Umlaute in HTML-Entities umsetzen
						case 'a' : wort += "&#228;"; break;
						case 'o' : wort += "&#246;"; break;
						case 'u' : wort += "&#252;"; break;
						case 'A' : wort += "&#196;"; break;
						case 'O' : wort += "&#214;"; break;
						case 'U' : wort += "&#220;"; break;
						case 's' : wort += "&#223;"; break;
						case '-' : break; // "- kein Text, Trennmarkierung
						case '\'': wort += "&quot;"; break; // "' deutsche Ende-Quotes
						case '`' : wort += "&quot;"; break; // "` deutsche Anfang-Quotes
						case '=' : wort += "-"; break; // "= Bindestrich an dem getrennt werden darf
						case '~' : wort += "-"; break; // "~ Bindestrich an dem nicht getrennt werden darf
						case '\n' : wort += "&quot;"; break; // " am Zeilenende
						// oder Fehler
						default :
							resetChar();
							output("[COMMAND] *** Umlaut zu ");
							output(c);
							output(" nicht definiert *** \n");
							wort += tag("text", "&quot;");
					}
				}
				else {
					wort += "&quot;"; break;
				}
			break;

			/// Fall '$' -> Mathematikmodus /// todo: \[, \{ !!!
			case '$' :
				satz += trim(tag("text", wort));
				wort = "";
				c = readChar();
				switch (c) {
					case '$':
						satz += trim(tag("command", "begin"));
						satz += tag("param", "displaymath");
						satz += parseFormula();
						satz += trim(tag("command", "end"));
						satz += tag("param", "displaymath");
						readChar();
					break;
					default:
						resetChar();
						satz += trim(tag("command", "begin"));
						satz += tag("param", "math");
						satz += parseFormula();
						satz += trim(tag("command", "end"));
						satz += tag("param", "math");
				}
			break;
	 
			/// Fall: "&" -> Tabular Umgebung ///
			case '&':
				satz += trim(tag("text" ,wort));
				wort = "";			
				satz += tag("command", "tablejump");
			break;

			/// Fall: "~" -> Silbentrennung ///
			case '~':
				wort += " ";
			break;

			/// +++ sonstige Fälle -> an Text anfügen +++ ///
			default :	wort += c;
		}
	
	} while (c!=0);

	return satz;
}	
	
// ==Parser== [COMMAND] oder Sonderzeichen (alles was mit \ ) beginnt
// return mit Tags
string Xparser::parseCommandOrSpecialChars() {

	string txt = "";
	char c;
	string dummy;

	c = readChar();
	
	switch (c) {
		/// Fall '\' -> Absatz ///
		case '\\' : {
			char c;
			c = readChar();
			switch (c) {
				// Zeilenumbruch ohne Seitenumbruch //*
				case '*' : break;
				// Zeilenumbruch mit Hhenangabe
				case '[' :
					parseParameter(c);
					//output("[COMMAND] *** Hoehenangabe bei <br/> noch nicht implementiert *** \n");
				break;
				default:
					resetChar();
			}
			txt += tag("command", "newline");
		} break;

		/// Fälle die zu einem (tw. untrennbarem) Leerzeichen oder gar keinem Zeichen werden
		/// (zb. weil es keine DocBook Entsprechung gibt)
		/// weiterhin alle Escape-Befehle
		case ' ' : txt += tag("text", "&#32;");	break; // Raum der nicht verbreitert werden darf
		case ',' : txt += tag("text", "&nbsp;"); break; // kleiner Raum
		case '-' : break; // forcierte Trennung
		case '/' : break; // Ligatur auflösen
		case '&' : txt += tag("text", "&amp;"); break;
		case '#' : txt += tag("text", "#"); break;
		case '$' : txt += tag("text", "$"); break;
		case '%' : txt += tag("text", "%"); break;
		case '_' : txt += tag("text", "_"); break;
		case '{' : txt += tag("text", "{"); break;
		case '}' : txt += tag("text", "}"); break;
		
		case '>' : txt += tag("command", "tabjump"); break;		
		case '=' : txt += tag("command", "tabmark"); break;					 

		/// Fälle '"' -> Umlaute ///
		case '"' : {
			char c;
			string dummy;
			c = readChar();
			switch (c) {
				// Umlaute in HTML-Entities umsetzen
				case 'a' : txt += tag("text", "&#228;"); break;
				case 'o' : txt += tag("text", "&#246;"); break;
				case 'u' : txt += tag("text", "&#252;"); break;
				case 'A' : txt += tag("text", "&#196;"); break;
				case 'O' : txt += tag("text", "&#214;"); break;
				case 'U' : txt += tag("text", "&#220;"); break;
				case 's' : txt += tag("text", "&#223;"); break;
				// oder Fehler
				default :
					resetChar();
					// output("[COMMAND] *** Umlaut zu ");
					// output(c);
					// output(" nicht definiert *** \n");
					// Diese Fehler sind gar keine: \"wort\" ist legitim und wird so zu "wort" :
					txt += tag("text", "&quot;");
			}
		} break;

		/// sonstige Fälle -> [COMMAND] ///
		default : {
			resetChar();
			txt += parseCommand();
		}
	}
	return txt;
}
	
// ==Parser== [COMMAND]
// return mit Tags
string Xparser::parseCommand() {

	string txt = "";
	char c;
	string dummy = "";
	string buf = "";

	do {
		c = readChar();

		// Fall: verb (nicht verbatim)
		if ((dummy == "verb") && (c != 'a')) {
			txt += tag("command", dummy);
			txt += parseParameter(c);
			c = 0; // Befehl beenden
		}	 

		// Fall: usepackage
		if ((dummy == "usepackage")) {
			txt += tag("command", dummy);
			while (c == '[') {
				txt += parseParameter(c);
				c = readChar();
			}
			buf = parseParameter(c);
			txt += buf;
			// 13 = <param><text>
			// 28 = <param><text> + </param></text>
			buf = buf.substr(13, buf.length()-28); 
			usePackage(buf);
			c = 0; // Befehl beenden
		}

		// Fall: input
		if (dummy == "input") {
			Xparser* NewParser;
			string infile;
		
			infile = parseParameter(c);
			// infile = <param><text>file.text</text></param>
			infile = infile.substr(13, infile.length()-28);
			// infile = file.text
			infile = Xparser::filename.substr(0, Xparser::filename.find_last_of("/\\") + 1) + infile;
			// infile = [vrz/]file.text

			NewParser = new Xparser();

			if (!NewParser->openFile(infile)) {
				output("[COMMAND] input - Datei '" + infile + "' nicht gefunden!\n");
				exit(2);
			}

			txt += NewParser->parse();
			output("(" + intToStr(txt.length()) + " Bytes)\n");
			NewParser->closeFile();
			
			delete NewParser;
			
			c = 0; // Befehl beenden
		} 
		
		/// Fälle: Buchstaben -> an Text anfügen ///
		if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
			dummy += c;
		}
		/// Fälle die Parameter nach sich ziehen ///
		else if (c == '{' || c == '(' || c == '[') {
			txt = tag("command", dummy);
			dummy = "";
			/// findet (theoretisch) alle Parameter
			while ((c == '{' || c == '(' || c == '[')) {
				txt += parseParameter(c);
				c = readChar();
			}
			resetChar();
			c = 0; // Befehlswort beendet
		}
		/// Fälle die Befehlswort beenden -> Ende ///
		else if (c == '\\' || c == ' ' || c == '\n'|| c == '\r' || c == '^' ||
				c == '"' || c == '~' || c == '}' ||	c == ')' || c == ']' ||
				c == '/' || c == '.' || c == '-' || c == '?' || c == '&' ||
				c == '$' || c == '@' || c == ',' || (c >= '0' && c <= '9')) {
			txt = tag("command", dummy);
			if (c != ' ' && c != '\n' && c != '\r') {
				resetChar();
			}
			dummy = "";
			c = 0; // Befehlswort beendet
		}
		/// Fälle die Befehlswort nicht beenden -> anfügen ///
		else if (c == '#' || c == '`' || c == '*') {
			dummy += c;
		}
		/// Fall = -> Befehl beenden, Parameter einlesen bis " " oder Sonderzeichen ///
		else if (c == '=') {
			// noch nicht korrekt implementiert -> Parameter geht bis " "
			txt = tag("command", dummy);
			dummy = "";			
			txt += parseParameter(' ');			
			c = 0; // Befehlswort beendet			
		}
		/// Fall 0 -> Befehl beendet ///
		else if (c == 0) {
		}
		/// Fälle die noch nicht geklärt sind ///
		else {
			dummy += c;
			//txt += tag("command",dummy);
			output("[COMMAND] *** fraglich: " + dummy + " *** \n");
		}

	} while (c != 0);
	return txt;
}

// ==Parser== [PARAMxx]
// return mit Tags
string Xparser::parseParameter(char klammerauf) {

	string txt = "";
	char c,klammerzu;
	string satz = "";
	string wort = "";
	string mode	= "PARAMxx";

	switch (klammerauf) {
		case '[' : klammerzu = ']'; break;			
		case '{' : klammerzu = '}'; break;		
		case '(' : klammerzu = ')'; break;
		default : klammerzu = klammerauf;
	}

	mode[5] = klammerauf;
	mode[6] = klammerzu;
	
	do {
		c = readChar();

		switch (c) {

			/// Fälle Klammern -> [PARAMxx] ///
			case '{'	:
			case '['	:
				satz += trim(tag("text", wort));
				wort = "";
				satz += parseParameter(c);
			break;
			
			// +++ die folgenden Fälle sind auch in ::parse !!! +++ //			

			/// Fall '%' -> [COMMENT] ///
			case '%' :
				satz += trim(tag("text", wort));
				wort = "";
				satz += parseComment();
			break;

			/// Fälle '\' -> [COMMAND] oder Sonderzeichen ///
			case '\\' :
				satz += trim(tag("text", wort));
				wort = "";
				satz += parseCommandOrSpecialChars();
			break;

			/// Fälle '"' -> Umlaute unter german ///
			case '"' :
				if (activeDblQuote == true) {
					char c;
					string dummy;
					c = readChar();
					switch (c) {
						// Umlaute in HTML-Entities umsetzen
						case 'a' : wort += "&#228;"; break;
						case 'o' : wort += "&#246;"; break;
						case 'u' : wort += "&#252;"; break;
						case 'A' : wort += "&#196;"; break;
						case 'O' : wort += "&#214;"; break;
						case 'U' : wort += "&#220;"; break;
						case 's' : wort += "&#223;"; break;
						case '-' : break; // "- kein Text, Trennmarkierung
						case '\'': wort += "&quot;"; break; // "' deutsche Ende-Quotes
						case '`' : wort += "&quot;"; break; // "` deutsche Anfang-Quotes
						case '=' : wort += "-"; break; // "= Bindestrich an dem getrennt werden darf
						case '~' : wort += "-"; break; // "= Bindestrich an dem getrennt nicht werden darf
						case '\n' : wort += "&quot;"; break; // " am Zeilenende
						// oder Fehler
						default :
							resetChar();
							resetChar();
							output("[COMMAND] *** Umlaut zu ");
							output(c);
							output(" nicht definiert *** \n");
					}
				}
				else {
					wort += "&quot;"; break;
				}
			break;
			
			/// Fall '$' -> Mathematikmodus /// todo: \[, \{ !!!
			case '$' :
				satz += trim(tag("text", wort));
				wort = "";
				c = readChar();
				switch (c) {
					case '$':
						satz += trim(tag("command", "begin"));
						satz += tag("param", "displaymath");
						satz += parseFormula();
						satz += trim(tag("command", "end"));
						satz += tag("param", "displaymath");
						readChar();
					break;
					default:
						resetChar();
						satz += trim(tag("command", "begin"));
						satz += tag("param", "math");
						satz += parseFormula();
						satz += trim(tag("command", "end"));
						satz += tag("param", "math");
				}
			break;
	 
			/// Fall: "&" -> Tabular Umgebung ///
			case '&':
				satz += trim(tag("text", wort));
				wort = "";			
				satz += tag("command", "tablejump");
			break;			

			/// Fall: "~" -> Silbentrennung ///
			case '~':
				wort += " ";
			break;
			
			/// +++ sonstige Fälle -> Ende oder an Text anfügen +++ ///
			default :
				if (klammerzu == c) {
					//satz += trim(wort);
					satz += trim(tag("text", wort));
					txt += satz;
					c = 0; // schließende Klammer beendet Parameter;
				} else {
					wort += c;
				}
			break;	
		}
	} while (c != 0);	
	
	return tag("param", txt);
}

// ==Parser== - [COMMENT]
// return mit Tags
string Xparser::parseComment() {

	string txt = "";
	string dummy = "";
	char c;
	
	do {
		c = readChar();

		switch (c) {
			/// Zeichen, die XML nicht mag ///
			case '<' : txt += "&lt;"; break;
			case '>' : txt += "&gt;"; break;

			/// Fall '\n' -> Ende ///
			case '\n' :
			case '\r' :			
				txt = dummy;
				c = 0; // Zeilenende beendet Kommentar
			break;
			
			/// sonstige Fälle -> an Text anfügen ///
			default :	dummy += c;
		}	
	} while (c != 0);
	
	return tag("comment", txt);
}

// ==Parser== - [FORMULA]
// return mit Tags
string Xparser::parseFormula() {

	string txt = "";
	string wort = "";
	char c;

	do {
		c = readChar();

		switch (c) {
			/// Fall '$' -	Formel zuende ///
			case '$' :
				txt = wort;
				c = 0; // beendet Formel
			break;

			/// Fall '\' - Formelbefehl ///
			case '\\' :
				c = readChar();
				switch (c) {
					case '$' : wort += "$"; break;
					default: resetChar();
				} 
			break;
			
			case '<' : wort += "&lt;"; break;
			case '>' : wort += "&gt;"; break;
			
			/// sonstige Fälle -> an Text anfügen ///
			default :	wort += c;
		}
	} while (c != 0);

	return tag("text", txt);
}