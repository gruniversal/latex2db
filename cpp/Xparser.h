#include <cstdlib>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

using namespace std;

class Xparser {
    static int activeDblQuote;
  private:
    ifstream ihandle;   // Dateihandle Eingang
    ofstream ohandle;   // Dateihandle Ausgang
    bool file_open;     // Datei offen ?
    string filename;    // Dateiname
    char inchar;        // letztes gelesenes Zeichen
    string parse();
    string parseCommandOrSpecialChars();
    string parseCommand();
    string parseParameter(char klammerauf);
    string parseComment();
    string parseFormula();
    char readChar();
    void resetChar();
    char peekChar();
    void readChars(char* &s,int maxlength,char delimiter);
    void readLine(char* &s,int maxlength);
    string intToStr(int number);
    void output(string txt);
    void output(int txt);
    void output(char txt);
    string tag(string name);
    string tag(string name,string inner);
    string trim(string s);
    int usePackage(string package);
  public:
    Xparser();
    ~Xparser();
    bool openFile(string filename);
    void closeFile();
    int convert(string filename);
};

