
  General Information:
  --------------------
  
  This package contains a C++ based LaTeX parser, which exports the LaTeX
  markup into an XML-file. It's the first half of the LaTeX2DocBook process.

  The project starter/leader is David Gruner (sourceforge: dave1980)  


  Content:
  --------
  
  At the moment this package contains only the class XParser, which does the
  work, and a frame program, that is used to read the command-line parameters.
  Both are provided as cpp sources and can be compiled with g++ which is
  part of the GNU Compiler Collection (gcc).

  
  License:
  --------
  
  The code is released under the GPL: http://www.gnu.org/copyleft/gpl.html
  
  Therefore you have the freedom to:
  
  1. use it for any purpose
  2. distribute a copy of the source-code
  3. modify the program to your needs
  4. distribute a copy of your modified version under the GPL license
  
  
  Language Notice:
  ----------------
  
  Because of my territorial origins all text (UI, documentation) is in german.  
  