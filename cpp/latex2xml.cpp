#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

#include "Xparser.h"

#define _version "0.718 (g++)"

using namespace std;

int main(int argc, char *argv[]) {
	// vars
	string infile = "", outfile = "";
	Xparser* Parser;
	int rcode;

	// Logo
	cout << "latex2xml v" << _version << " by David Gruner" << endl;

	// Parameter
	if (argc == 2) {
		// In: %1 Out: %1.xml
		infile = argv[1];
		outfile = infile + ".xml";
	}
	else if (argc == 3) {
		// In: %1 Out: %2
		infile = argv[1];
		outfile = argv[2];
	}
	else {
		// Hilfe anzeigen
		cout << "Parameter: latex2xml Quelldatei [Zieldatei]" << endl;
		exit(1);
	}
	
	cout << "konvertiere " << infile << " zu " << outfile << " .." << endl;

	// Los
	Parser = new Xparser();

	if (!Parser->openFile(infile)) {
		cout << "Fehler: Datei " << infile << " nicht gefunden!" << endl;
		exit(2);
	}
	rcode = Parser->convert(outfile);
	if (rcode>-1) {
		cout << endl;
		cout << "Konvertierung erfolgreich beendet." << endl;
		cout << rcode+1 << " Bytes nach " << outfile << " geschrieben" << endl;
	} 
	Parser->closeFile();

	delete Parser;
	
	// getchar();
	exit(0);
}
