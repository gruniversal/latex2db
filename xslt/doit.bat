@echo off

if "%2" == "1" goto step1
if "%2" == "2" goto step2
if "%2" == "3" goto step3
if "%2" == "4" goto step4
if "%2" == "5" goto step5
if "%2" == "6" goto step6
if "%2" == "7" goto step7
if "%2" == "8" goto step8

:start

echo.

:step1
echo [XSLT1] Entferne Kommentare ..
call xsltproc -o %1.xsl1.xml xsl1_delete_comments.xsl %1.xml

if "%3" == "1" goto ende

:step2
echo [XSLT2] Ordne Parameter den Befehlen zu ..
call xsltproc -o %1.xsl2.xml xsl2_subordinate_params.xsl %1.xsl1.xml

if "%3" == "2" goto ende

:step3
echo [XSLT3] Ersetze Text-Befehle ..
call xsltproc -o %1.xsl3.xml xsl3_replace_textcommands.xsl %1.xsl2.xml

if "%3" == "3" goto ende

:step4
echo [XSLT4] Fuehre Texte zusammen ..
call xsltproc -o %1.xsl4.xml xsl4_merge_texts.xsl %1.xsl3.xml

if "%3" == "4" goto ende

:step5
echo [XSLT5] Ersetze Strukturbefehle ..
call xsltproc -o %1.xsl5.xml xsl5_replace_structural_commands.xsl %1.xsl4.xml

if "%3" == "5" goto ende

:step6
echo [XSLT6] Ordne Texte den Sektionen unter ..
call xsltproc -o %1.xsl6.xml xsl6_subordinate_sections.xsl %1.xsl5.xml

if "%3" == "6" goto ende

:step7
echo [XSLT7] Ordne Texte den Unter-Sektionen unter ..
call xsltproc -o %1.xsl7.xml xsl7_subordinate_subsections.xsl %1.xsl6.xml

if "%3" == "7" goto ende

:step8
echo [XSLT8] Ordne Texte den Unter-Unter-Sektionen unter ..
call xsltproc -o %1.xsl8.xml xsl8_subordinate_subsubsections.xsl %1.xsl7.xml

if "%3" == "8" goto ende

:ende