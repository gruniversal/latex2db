<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<xsl:output method="xml"/>

<!-- "main" -->
<xsl:template match="/">
  <latexxml>
  <xsl:apply-templates/>
  </latexxml>
</xsl:template>

<!-- text -->
<xsl:template match="text">  
  <xsl:copy-of select=".">
    <xsl:apply-templates/>
  </xsl:copy-of> 
</xsl:template>

<!-- param -->
<xsl:template match="param">  
  <xsl:variable name="prev" select="position()-1"/> 
  <xsl:variable name="lname" select="local-name(../*[position() = $prev])"/> 
	<xsl:if test="not($lname = 'param' or $lname = 'command')">
		<xsl:text disable-output-escaping="yes">&lt;command name="block"&gt;</xsl:text>  
	</xsl:if>    
  <param>
    <xsl:apply-templates/>
  </param>
  <xsl:variable name="next" select="position()+1"/> 
	<xsl:if test="not(local-name(../*[position() = $next]) = 'param')">
		<xsl:text disable-output-escaping="yes">&lt;/command&gt;</xsl:text>  
	</xsl:if>    
</xsl:template>

<!-- command -->
<xsl:template match="command">  
  <xsl:text disable-output-escaping="yes">&lt;command name="</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text disable-output-escaping="yes">"&gt;</xsl:text>
  <xsl:variable name="next" select="position()+1"/> 
	<xsl:if test="not(local-name(../*[position() = $next]) = 'param')">
		<xsl:text disable-output-escaping="yes">&lt;/command&gt;</xsl:text>  
	</xsl:if>    
</xsl:template>


</xsl:stylesheet>

