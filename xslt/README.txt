
  General Information:
  --------------------
  
  This package contains some XSLT stylesheets, which transform an XML-file
  (written with latex2xml) into the DocBook format. This is the second half
  of the LaTeX2DocBook process.

  The project starter/leader is David Gruner (sourceforge: dave1980)  


  Content:
  --------
  
  At the moment this package contains eight XSLT-stylesheets, that do some
  basic transformations to translate the given (LaTeX-)XML into DocBook XML.
  Additionally there are two windows batch files, which can be used to
  perform the XSLT-steps in a batch.

  
  License:
  --------
  
  The code is released under the GPL: http://www.gnu.org/copyleft/gpl.html
  
  Therefore you have the freedom to:
  
  1. use it for any purpose
  2. distribute a copy of the source-code
  3. modify the program to your needs
  4. distribute a copy of your modified version under the GPL license
  
  
  Language Notice:
  ----------------
  
  Because of my territorial origins all text (UI, documentation) is in german.  
  