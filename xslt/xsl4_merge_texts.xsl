<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<xsl:output method="xml"/>

<!-- "main" -->
<xsl:template match="/">
  <latexxml>
  <xsl:apply-templates/>
  </latexxml>
</xsl:template>

<!-- texte zu paras -->
<xsl:template match="text">
  <xsl:variable name="prev" select="position()-1"/>
  <xsl:variable name="next" select="position()+1"/>
  <xsl:if test="not(local-name(../*[position() = $prev]) = 'text')">
    <xsl:text disable-output-escaping="yes">&lt;para&gt;</xsl:text>
  </xsl:if>
  <xsl:value-of select="./text()"/>
  <xsl:copy-of select="./*"/>
  <xsl:if test="not(local-name(../*[position() = $next]) = 'text')">
    <xsl:text disable-output-escaping="yes">&lt;/para&gt;</xsl:text>
  </xsl:if>
</xsl:template>

<!-- commands kopieren (params werden entfernt!) -->
<xsl:template match="command">
  <xsl:variable name="ename" select="local-name()"/>
  <xsl:variable name="name" select="@name"/>
  <command name="{$name}">
    <xsl:apply-templates/>
  </command>
</xsl:template>

</xsl:stylesheet>

