<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<xsl:output method="xml"/>

<!-- "main" -->
<xsl:template match="/">
  <latexxml>
  <xsl:apply-templates/>
  </latexxml>
</xsl:template>

<!-- alle behandelten Text-Commands in alphabetischer Reihenfolge -->

<xsl:template match="command[@name = 'emph']">
  <text><emphasis>
    <xsl:value-of select="./param/text"/>
  </emphasis></text>
</xsl:template>

<xsl:template match="command[@name = 'textbf']">
  <text><emphasis role="bold">
    <xsl:value-of select="./param/text"/>
  </emphasis></text>
</xsl:template>

<xsl:template match="command[@name = 'textit']">
  <text><emphasis role="italic">
    <xsl:value-of select="./param/text"/>
  </emphasis></text>
</xsl:template>

<xsl:template match="command[@name = 'textbackslash']">
  <text>&#92;</text>
</xsl:template>

<xsl:template match="command[@name = 'textgreater']">
  <text>&gt;</text>
</xsl:template>

<xsl:template match="command[@name = 'textless']">
  <text>&lt;</text>
</xsl:template>

<xsl:template match="command[@name = 'dq']">
  <text>&quot;</text>
</xsl:template>

<xsl:template match="command[@name = 'dots']">
  <text>...</text>
</xsl:template>

<xsl:template match="command[@name = 'verb']">
  <text><literal>
    <xsl:value-of select="./param/text"/>
  </literal></text>
</xsl:template>

<xsl:template match="command[@name = 'url']">
  <text><xsl:text disable-output-escaping="yes">&lt;ulink url="</xsl:text>
    <xsl:value-of select="./param/text"/>
  <xsl:text disable-output-escaping="yes">"&gt;&lt;/ulink&gt;</xsl:text></text>
</xsl:template>


<!-- Umbruchsteuerung -->
<xsl:template match="command[@name = 'nobreak']"/>

<!-- Tabbing Umgebung - derzeit einfach rausnehmen -->
<xsl:template match="command[@name = 'tabjump']"/>
<xsl:template match="command[@name = 'tabmark']"/>

<!-- LaTeX / TeX - spezielle Latex Macros -->
<xsl:template match="command[@name = 'LaTeX']">
  <text>LaTeX</text>
</xsl:template>

<xsl:template match="command[@name = 'TeX']">
  <text>TeX</text>
</xsl:template>

<!-- footnote - ! muss nach xsl3 und vor xsl4 kommen :( -->
<xsl:template match="command[@name = 'footnote']">
  <text><footnote><para>
  <xsl:value-of select="./param/text"/>
  </para></footnote></text>
</xsl:template>

<!-- cite -->
<xsl:template match="command[@name = 'cite']">
  <text><citation>
    <xsl:for-each select="./param/text">
      <xsl:variable name="pos" select="position()"/>
      <xsl:value-of select="."/>
      <xsl:if test="last() > $pos">
        <xsl:text>, </xsl:text>
      </xsl:if>
    </xsl:for-each>
  </citation></text>
</xsl:template>

<!-- ref -->
<xsl:template match="command[@name = 'ref']">
  <text>
  <xsl:element name="xref">
    <xsl:attribute name="linkend">
      <xsl:value-of select="./param/text"/>
    </xsl:attribute>
  </xsl:element>
  </text>
</xsl:template>

<!-- pageref - gibt es in Docbook nicht -->
<xsl:template match="command[@name = 'pageref']">
  <text>???</text>
</xsl:template>

<!-- Bloecke - derzeit keine Bearbeitung, einfach rausnehmen -->
<xsl:template match="command[@name = 'block']">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="command[@name = 'block']/param">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="command[@name = 'tt']"/>

<!-- Boxen - derzeit keine Bearbeitung, einfach rausnehmen - ->
<xsl:template match="command[@name = 'scalebox']">
  <xsl:apply-templates select="./param/command"/>
</xsl:template>

<xsl:template match="command[@name = 'scalebox']/param"/>

-->

<!-- fbox - nur Argument rausnehmen -->
<xsl:template match="command[@name = 'fbox']">
  <xsl:apply-templates select="./param"/>
</xsl:template>

<xsl:template match="command[@name = 'fbox']/param">
  <xsl:apply-templates/>
</xsl:template>

<!-- parbox - bis aufs letzte Argument rausnehmen -->
<xsl:template match="command[@name = 'parbox']">
  <xsl:apply-templates select="./param"/>
</xsl:template>

<xsl:template match="command[@name = 'parbox']/param">
  <xsl:if test="(position()=last())">
    <xsl:apply-templates/>
  </xsl:if>
</xsl:template>

<!-- index: hyperpage -->
<xsl:template match="command[@name = 'hyperpage']">
  <xsl:apply-templates select="./param"/>
</xsl:template>

<xsl:template match="command[@name = 'hyperpage']/param">
  <xsl:apply-templates/>
</xsl:template>


<!-- alle Texte kopieren -->

<xsl:template match="text">
  <xsl:copy-of select=".">
    <xsl:apply-templates/>
  </xsl:copy-of>
</xsl:template>

<!-- alle verbliebenen commands/params werden kopiert -->

<xsl:template match="command">
  <xsl:element name="command">
    <xsl:attribute name="name">
      <xsl:value-of select="@name"/>
    </xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="param">
  <xsl:element name="param">
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>

