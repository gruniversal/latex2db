<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<xsl:output method="xml"/>

<!-- comments loeschen -->
<xsl:template match="comment"/>

<!-- alles andere kopieren -->
<xsl:template match="command|text|param|latexxml">
  <xsl:variable name="name" select="local-name()"/>
  <xsl:element name="{$name}">
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>

