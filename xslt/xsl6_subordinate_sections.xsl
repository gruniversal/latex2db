<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<xsl:output method="xml"/>

<!-- "main" -->
<xsl:template match="/">
  <xsl:apply-templates/>
</xsl:template>

<!-- section -->

<xsl:template match="section[1]">
  <xsl:text disable-output-escaping="yes">&lt;sect1&gt;</xsl:text>
  <xsl:text disable-output-escaping="yes">&lt;title&gt;</xsl:text>
  <xsl:value-of select="@title"/>
  <xsl:text disable-output-escaping="yes">&lt;/title&gt;</xsl:text>
</xsl:template>

<xsl:template match="section[@title='dummy']">
  <subsection title="dummy"/>
  <xsl:text disable-output-escaping="yes">&lt;/sect1&gt;</xsl:text>
</xsl:template>

<xsl:template match="section">
  <subsection title="dummy"/>
  <xsl:text disable-output-escaping="yes">&lt;/sect1&gt;</xsl:text>
  <xsl:text disable-output-escaping="yes">&lt;sect1&gt;</xsl:text>
  <xsl:text disable-output-escaping="yes">&lt;title&gt;</xsl:text>
  <xsl:value-of select="@title"/>
  <xsl:text disable-output-escaping="yes">&lt;/title&gt;</xsl:text>
</xsl:template>

<!-- item -->

<xsl:template match="item">
  <xsl:choose>
    <xsl:when test="local-name(..) = 'variablelist'">
      <xsl:variable name="pos" select="position()"/>
      <xsl:if test="$pos > 1">
        <xsl:text disable-output-escaping="yes">&lt;/listitem&gt;</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;/varlistentry&gt;</xsl:text>
      </xsl:if>
      <xsl:if test="last() > $pos">
        <xsl:text disable-output-escaping="yes">&lt;varlistentry&gt;</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;term&gt;</xsl:text>
        <xsl:value-of select="@term"/>
        <xsl:text disable-output-escaping="yes">&lt;/term&gt;</xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;listitem&gt;</xsl:text>
      </xsl:if>
    </xsl:when>
    <xsl:when test="local-name(..) = 'index'">
      <xsl:variable name="pos" select="position()"/>
      <xsl:if test="$pos = 1">
        <title>Index</title>
      </xsl:if>
      <xsl:if test="$pos > 1">
        <xsl:text disable-output-escaping="yes">&lt;/indexentry&gt;</xsl:text>
      </xsl:if>
      <xsl:if test="last() > $pos">
        <xsl:text disable-output-escaping="yes">&lt;indexentry&gt;</xsl:text>
      </xsl:if>
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="pos" select="position()"/>
      <xsl:if test="$pos > 1">
        <xsl:text disable-output-escaping="yes">&lt;/listitem&gt;</xsl:text>
      </xsl:if>
      <xsl:if test="last() > $pos">
        <xsl:text disable-output-escaping="yes">&lt;listitem&gt;</xsl:text>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- figure -->

<xsl:template match="figure">
  <xsl:element name="figure">
    <xsl:attribute name="id">
      <xsl:value-of select="./anchor/@id"/>
    </xsl:attribute>
    <title>
      <xsl:value-of select="./caption"/>
    </title>
    <xsl:if test="count(./includegraphics) &gt; 0">
      <xsl:element name="graphic">
        <xsl:attribute name="fileref">
          <xsl:value-of select="./includegraphics"/>
        </xsl:attribute>
      </xsl:element>
    </xsl:if>
    <xsl:if test="count(./para) &gt; 0">
      <xsl:element name="informalexample">
        <xsl:for-each select="./para">
          <xsl:copy-of select="."/>
        </xsl:for-each>
      </xsl:element>
    </xsl:if>
  </xsl:element>
</xsl:template>

<!-- alles andere kopieren -->

<!-- kopiert ohne apply -->
<xsl:template match="para|subsection|subsubsection|anchor">
  <xsl:copy-of select="."/>
</xsl:template>

<!-- kopiert mit apply -->
<xsl:template match="article|book|itemizedlist|orderedlist|variablelist|index">
  <xsl:variable name="name" select="local-name()"/>
  <xsl:element name="{$name}">
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>

