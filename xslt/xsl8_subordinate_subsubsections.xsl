<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<xsl:output method="xml"/>

<!-- "main" -->
<xsl:template match="/">
  <xsl:apply-templates/>
</xsl:template>

<!-- section -->

<xsl:template match="sect2/subsubsection[1]">
  <xsl:text disable-output-escaping="yes">&lt;sect3&gt;</xsl:text>
  <xsl:text disable-output-escaping="yes">&lt;title&gt;</xsl:text>
  <xsl:value-of select="@title"/>
  <xsl:text disable-output-escaping="yes">&lt;/title&gt;</xsl:text>
</xsl:template>

<xsl:template match="subsubsection[@title='dummy']">
  <xsl:if test="count(../subsubsection) > 1">
    <xsl:text disable-output-escaping="yes">&lt;/sect3&gt;</xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="subsubsection">
  <xsl:text disable-output-escaping="yes">&lt;/sect3&gt;</xsl:text>
  <xsl:text disable-output-escaping="yes">&lt;sect3&gt;</xsl:text>
  <xsl:text disable-output-escaping="yes">&lt;title&gt;</xsl:text>
  <xsl:value-of select="@title"/>
  <xsl:text disable-output-escaping="yes">&lt;/title&gt;</xsl:text>
</xsl:template>


<!-- alles andere kopieren -->

<!-- kopiert ohne apply -->
<xsl:template match="para|itemizedlist|orderedlist|variablelist|index|figure|anchor">
  <xsl:copy-of select="."/>
</xsl:template>

<!-- kopiert mit apply -->
<xsl:template match="article|book|sect1|sect2|title">
  <xsl:variable name="name" select="local-name()"/>
  <xsl:element name="{$name}">
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>

