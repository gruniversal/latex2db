<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<xsl:output method="xml"/>

<!-- "main" -->
<xsl:template match="/">
  <xsl:apply-templates/>
</xsl:template>

<!-- begin / end -->

<xsl:template match="command[@name = 'begin']">
  <xsl:variable name="env" select="./para"/>
  <xsl:choose>
    <xsl:when test="$env = 'document'">
      <xsl:variable name="dclass" select="//command[@name = 'documentclass']/para[2]"/>
      <xsl:variable name="dstyle" select="//command[@name = 'documentstyle']/para[2]"/>
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:choose>
        <xsl:when test="$dclass = 'article' or $dstyle = 'article'">
          <xsl:text>article</xsl:text>
        </xsl:when>
        <xsl:when test="$dclass = 'book' or $dstyle = 'book'">
          <xsl:text>book</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>book</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'enumerate'">
      <xsl:text disable-output-escaping="yes">&lt;orderedlist&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'itemize'">
      <xsl:text disable-output-escaping="yes">&lt;itemizedlist&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'description'">
      <xsl:text disable-output-escaping="yes">&lt;variablelist&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'theindex'">
      <xsl:text disable-output-escaping="yes">&lt;index&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'figure'">
      <xsl:text disable-output-escaping="yes">&lt;figure&gt;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <!-- keinerlei Ausgabe => command loeschen -->
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="command[@name = 'end']">
  <xsl:variable name="env" select="./para"/>
  <xsl:choose>
    <xsl:when test="$env = 'document'">
      <xsl:variable name="dclass" select="//command[@name = 'documentclass']/para[2]"/>
      <xsl:variable name="dstyle" select="//command[@name = 'documentstyle']/para[2]"/>
      <xsl:if test="boolean(//command[@name = 'section'])">
        <section title="dummy"/>
      </xsl:if>
      <xsl:text disable-output-escaping="yes">&lt;/</xsl:text>
      <xsl:choose>
        <xsl:when test="$dclass = 'article' or $dstyle = 'article'">
          <xsl:text>article</xsl:text>
        </xsl:when>
        <xsl:when test="$dclass = 'book' or $dstyle = 'book'">
          <xsl:text>book</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>book</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'enumerate'">
      <xsl:text disable-output-escaping="yes">&lt;item/&gt;</xsl:text>
      <xsl:text disable-output-escaping="yes">&lt;/orderedlist&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'itemize'">
      <xsl:text disable-output-escaping="yes">&lt;item/&gt;</xsl:text>
      <xsl:text disable-output-escaping="yes">&lt;/itemizedlist&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'description'">
      <xsl:text disable-output-escaping="yes">&lt;item/&gt;</xsl:text>
      <xsl:text disable-output-escaping="yes">&lt;/variablelist&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'theindex'">
      <xsl:text disable-output-escaping="yes">&lt;item/&gt;</xsl:text>
      <xsl:text disable-output-escaping="yes">&lt;/index&gt;</xsl:text>
    </xsl:when>
    <xsl:when test="$env = 'figure'">
      <xsl:text disable-output-escaping="yes">&lt;/figure&gt;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <!-- keinerlei Ausgabe => command loeschen -->
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- sonstige Commands -->

<!-- sections -->
<xsl:template match="command[@name = 'section']">
  <section title="{./para}"/>
</xsl:template>

<xsl:template match="command[@name = 'subsection']">
  <subsection title="{./para}"/>
</xsl:template>

<xsl:template match="command[@name = 'subsubsection']">
  <subsubsection title="{./para}"/>
</xsl:template>

<!-- listen -->
<xsl:template match="command[@name = 'item']">
  <xsl:text disable-output-escaping="yes">&lt;item </xsl:text>
  <xsl:if test="boolean(./para)">
    <xsl:text disable-output-escaping="yes">term=&quot;</xsl:text>
    <xsl:value-of select="./para"/>
    <xsl:text disable-output-escaping="yes">&quot;</xsl:text>
  </xsl:if>
  <xsl:text disable-output-escaping="yes">/&gt;</xsl:text>
</xsl:template>

<!-- grafiken -->
<xsl:template match="command[@name = 'label']">
  <xsl:text disable-output-escaping="yes">&lt;label&gt;</xsl:text>
  <xsl:value-of select="./para"/>
  <xsl:text disable-output-escaping="yes">&lt;/label&gt;</xsl:text>
</xsl:template>

<xsl:template match="command[@name = 'caption']">
  <xsl:text disable-output-escaping="yes">&lt;caption&gt;</xsl:text>
  <xsl:value-of select="./para"/>
  <xsl:text disable-output-escaping="yes">&lt;/caption&gt;</xsl:text>
</xsl:template>

<xsl:template match="command[@name = 'includegraphics']">
  <xsl:text disable-output-escaping="yes">&lt;includegraphics&gt;</xsl:text>
  <xsl:value-of select="./para"/>
  <xsl:text disable-output-escaping="yes">&lt;/includegraphics&gt;</xsl:text>
</xsl:template>

<!-- Grafikzeugs raus -->
<xsl:template match="command[@name = 'scalebox']">
  <xsl:apply-templates select="./*[local-name() != 'para']"/>
</xsl:template>

<!-- label -->
<xsl:template match="command[@name = 'label']">
  <xsl:text disable-output-escaping="yes">&lt;anchor id="</xsl:text>
    <xsl:value-of select="./para"/>
  <xsl:text disable-output-escaping="yes">"/&gt;</xsl:text>
</xsl:template>

<!-- alle verbliebenen commands werden einfach geloescht -->

<xsl:template match="command"/>

<!-- alle Texte kopieren -->

<xsl:template match="para">
  <xsl:copy-of select=".">
    <xsl:apply-templates/>
  </xsl:copy-of>
</xsl:template>

</xsl:stylesheet>

